<?php

namespace App\DataTables;

use App\Models\Event;
use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UsersListDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
        ->eloquent($query)
        ->editColumn('name', function ($row) {
            $image = ($row->profilepic && Storage::exists('public/profile_pic/thumbnails/' . $row->profilepic)) ? Storage::url('public/profile_pic/thumbnails/' . $row->profilepic) : asset('images/avatar.jpg');
            
            return '<img src="' . $image . '" style="width: 64px;height: 64px;" class="material-icons opacity-10">'. $row->name;
        })
        ->editColumn('status', function ($row) {
            $class = 'secondary';
            $statusText = 'Inactive';
            if($row->status == 1) {
                $class = 'success';
                $statusText = 'Active';
            }
            return '<span class="badge badge-sm bg-gradient-'. $class . '">' . $statusText . '</span>';
        })
        ->editColumn('age', function($row) {
            return $row->dob ? \Carbon\Carbon::parse($row->dob)->diff(\Carbon\Carbon::now())->format('%y y %m m %d d') : '-';//\Carbon\Carbon::parse($row->dob)->age;
        })
        ->addColumn('action', function ($row) {
            return view('components.datatable_actions', ['id' => $row->id, 'path' => 'users.details']);
        })
        ->filter(function ($query) {
            if (request()->has('age')  && request('age') > -1) {
                $year = date('Y', strtotime(request('age') . ' years ago'));
                $query->whereYear('user_details.dob', $year);
            }
        })
        ->addIndexColumn()
        ->rawColumns(['status', 'age', 'action', 'name']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'users.status',
                'user_details.dob',
                'user_details.profilepic'
                )
            ->leftJoin('user_details', 'user_details.user_id', 'users.id');
            // ->except(Auth::guard('user')->id());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->setTableId('user-details-datatable')
        ->columns($this->getColumns())
        ->ajax([
            'data' => 'function(d) {
                d.age = $("#user-age").val() ? $("#user-age").val() : -1;
            }',
        ])
        ->dom("<'row'<'col-sm-6'B><'col-sm-6 text-right'l>>rt<'row'<'col-sm-6'p><'col-sm-6 text-right'i>>")
        ->orderBy(2, 'asc')
        ->buttons($this->generateButtons())
        ->parameters([
            "pagingType" => "simple_numbers",
            "language" => [
                'paginate' => [
                    'previous' => '<span aria-hidden="true" class="material-icons">chevron_left</span><span>Prev</span>',
                    'next' => 'NEXT<span aria-hidden="true" class="material-icons">chevron_right</span>',
                ],
                'info' => '_START_ <span class="text-50">of _TOTAL_ <em class="material-icons ml-1">arrow_forward</em></span>',
                'infoFiltered' => '',
                "infoEmpty" => "<span class='text-50'>No records available</span>",
            ],
            'initComplete' => "function () {
                let oTable = $('#user-details-datatable').DataTable(),
                    page_element = {
                        search: $('#filter-search'),
                        filter: $('#filter'),
                        status:  $('#filter-status'),
                        name: $('#user-name'),
                        email: $('#user-email'),
                        age: $('#user-age')
                    }

                // global search
                page_element.search.on('keyup', function() {
                    let keyword = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    oTable.column(1).search(keyword).draw() ;
                });

                // status dropdown filter event
                page_element.status.on('change', function() {
                    let status_value = $(this).val();
                    oTable.column(4).search( status_value ? status_value : ' ', true, false )
                    .draw();
                });

                // name dropdown filter event
                page_element.name.on('change', function() {
                    let value = $(this).val();
                    oTable.column(1).search( value ? value : ' ', true, false )
                    .draw();
                });

                // email dropdown filter event
                page_element.email.on('change', function() {
                    let value = $(this).val();
                    oTable.column(2).search( value ? value : ' ', true, false )
                    .draw();
                });

                // global search
                page_element.age.on('change', function() {
                    oTable.draw();
                });
                
           }",
            'responsive' => 'true',
            'drawCallback' => "function () {
                $('.dataTables_paginate > .pagination').addClass('justify-content-start pagination-xsm m-0');
            }",
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                ->title('#')
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->orderable(false),
                // 'Profile Image' => ['data'=>'profile_image', 'name' => 'user_details.profilepic', 'order' => false, 'search' => false],
                'Name' => ['data' => 'name', 'name' => 'users.name'],
                'Email' => ['data' => 'email', 'name' => 'users.email', 'search' => false],
                'Age'   => ['data' => 'age', 'name' => 'user_details.dob'],
                'Status' => ['data' => 'status', 'name' => 'users.status'],
                Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'user_list' . date('YmdHis');
    }

    /**
     * Generate buttons
     *
     * @author Siji <cgann02@gmail.com>
     *
     * @return array
     */
    protected function generateButtons(): array
    {
        return [
            Button::make('reset')
        ];
    }
}
