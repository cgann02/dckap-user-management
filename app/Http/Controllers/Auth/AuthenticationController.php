<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthenticationController extends Controller
{
    public function login()
    {
        if(Auth::guard('user')->user()) {
            return redirect()->route('home');
        }
        return view('auth.login');
    }

    /**
     * Google Login
     *
     * GET login/google
     */
    public function redirectToGoogle(Request $r)
    {
        if ($r->query('next_url')) {
            session()->put('next_url', $r->query('next_url'));
        }
        return Socialite::driver('google')->with(["prompt" => "select_account"])->redirect();
    }

    /**
     * Google Login Callback
     *
     * GET login/google/callback
     */
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();

            $finduser = UserDetail::where('google_id', $user->id)->first();
       
            if($finduser){
                
                Auth::guard('user')->loginUsingId($finduser->user_id);
      
                return redirect()->route('home');
       
            }else{
                $newUser = User::updateOrCreate(['email' => $user->email], [
                    'name' => $user->name,
                    'email_verified_at' => now()
                ]);
                // $newUser = User::create([
                //     'name' => $user->name,
                //     'email' => $user->email,
                //     'email_verified_at' => now()
                // ]);

                $newUser->userDetails()->updateOrCreate(['user_id' => $newUser->id], ['google_id' => $user->id]);

                Auth::guard('user')->loginUsingId($newUser->id);
      
                return redirect()->route('home');
            }
      
        } catch (Exception $e) {
            dd($e->getMessage());
        }

    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\View\View
     */
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();

        $request->session()->regenerateToken();
        return redirect()->route('login');
    }
}
