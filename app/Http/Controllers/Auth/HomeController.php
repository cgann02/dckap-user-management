<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index() {
        $user = User::all()->except(Auth::guard('user')->id());

        $inactiveUserCount = $user->where('status', 0)->count();
        $activeUserCount = $user->where('status', 1)->count();

        return view('dashboard', compact('inactiveUserCount', 'activeUserCount'));
    }
}
