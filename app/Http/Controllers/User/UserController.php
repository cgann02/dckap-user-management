<?php

namespace App\Http\Controllers\User;

use App\DataTables\UsersListDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserDetailsRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\EducationCategory;
use App\Models\User;
use App\Repositories\UserRepository;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(UsersListDataTable $dataTable)
    {
        $users = User::all()->except(Auth::user());
        return $dataTable->render('user.user-list', ['users' => $users]);
    }

    public function create()
    {
        $education = EducationCategory::all();
        $country = Country::all();
        return View('user.create-user', compact('education', 'country'));
    }

    public function getCountryBasedCities($countryId)
    {   
        try {

            $cities = City::where('country_id', $countryId)->pluck('name', 'id');
            
            if($cities->count()) {
                return response()->json([
                    'success' => true,
                    'message' => 'Cities listed successfully!',
                    'data' => $cities,
                    'code' => 200
                ], 200);
            }

            return response()->json([
                'success' => false,
                'message' => 'No details found with this country!',
                'errors' => [],
                'code' => 200
            ], 200);

            /** API through fetching city  names */
                // $countries = Country::find($countryId);
    
                // $client = new Client();
                // $response = $client->request('POST', 'https://countriesnow.space/api/v0.1/countries/cities', [
                //     'form_params' => [
                //         "country" => $countries->name//strtolower($value->name)
                //     ]
                // ]);
    
                // $response = $response->getBody()->getContents();
                // $data = json_decode($response);
                // foreach($data->data as $k => $value){
                //     $cityName[] = $value;
                // }

                // if(!$data->error) {

                //     return response()->json([
                //             'success' => true,
                //             'message' => $data->msg,
                //             'data' => $cityName,
                //             'code' => 200
                //         ], 200);
                // } 

                /** end */
            
        // } catch(ConnectException $e){
        //     return response()->json([
        //         'success' => false,
        //         'message' => $e->getMessage(),
        //         'errors' => [],
        //         'code' => $e->getCode()
        //     ], $e->getCode());
        // } catch(GuzzleException $guzzleException){
        //     $expection = $guzzleException;
        //     if ($expection instanceof RequestException) {
        //         $exceptionResponse = $expection->getResponse();
        //         if($exceptionResponse) {                    
        //             $this->statusCode = $exceptionResponse->getStatusCode();
        //             $this->message = $exceptionResponse->getReasonPhrase();
        //         }
        //     }
        //     return response()->json([
        //         'success' => false,
        //         'message' => $this->message,
        //         'errors' => [],
        //         'code' => $this->statusCode
        //     ], $this->statusCode);
        }
        catch(Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => [],
                'code' => $e->getCode()
            ], $e->getCode());

        }
    }

    public function store (StoreUserDetailsRequest $request)
    {
        try {

            $user = $this->userRepository->createNew($request->all());

            return response()->json([
                'success' => true,
                'message' => 'User Created Successfully!',
                'data' => $user,
                'code' => 200
            ], 200);
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => [],
                'code' => $e->getCode()
            ], $e->getCode());
        }
    }

    public function show($id) {

        $user = User::find($id);
        if($user) {
            $userDetails = $user->userDetails; 
            
            return View('user.view-user', compact('user', 'userDetails'));
        }
        
        return back()->with('commonError', 'User data not found!');
    }

    public function edit($id)
    {
        $user = User::find($id);
        if($user) {
            $userDetails = $user->userDetails; 
            
            $country = Country::all();
            $city = '';
            $education = EducationCategory::all();

            if($userDetails && $userDetails->country_id) {
                $city = City::where('country_id', $userDetails->country_id)->pluck('name', 'id');
            }

            return View('user.edit-user', compact('user', 'userDetails', 'country', 'city', 'education'));
        }
        
        return back()->with('commonError', 'User data not found!');

        
    }

    public function update (StoreUserDetailsRequest $request)
    {
        try {

            $user = $this->userRepository->updateUserInfo($request->user_id, $request->all());

            return response()->json([
                'success' => true,
                'message' => 'User Details Updated Successfully!',
                'data' => $user,
                'code' => 200
            ], 200);
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => [],
                'code' => $e->getCode()
            ], $e->getCode());
        }
    }

    public function delete($id)
    {
        try {

            $user = User::find($id)->delete();

            return response()->json([
                'success' => true,
                'message' => 'User deleted successfully!',
                'data' => $user,
                'code' => 200
            ], 200);
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => [],
                'code' => $e->getCode()
            ], $e->getCode());
        }
    }
}
