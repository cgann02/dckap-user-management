<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;

class StoreUserDetailsRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $validation = [
            'name' => 'required|string|max:100',
            'address' => 'required|max:500',
            // 'email' => 'required|unique:users,email',
            'dob' => 'required|date|date_format:d-m-Y',
            'status' => 'required',
            'education' => 'required|exists:education_categories,id',
            'country' => 'required|exists:countries,id',
            'city' => 'required|exists:cities,id,country_id,'.$this->input('country'),
            'pincode' => 'required',
        ];

        if(!$this->input('user_id')) {
            $validation['profilepic'] = 'required|image|mimes:jpeg,png,jpg|max:2048';
            $validation['email']      = 'required|unique:users,email';
        } else {
            $validation['email']      = 'required|unique:users,email,'.$this->input('user_id');
        }

        return $validation;
    }
}
