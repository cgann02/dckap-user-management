<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserDetail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'dob',
        'address',
        'education_id',
        'profilepic',
        'country_id',
        'pincode',
        'city_id'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'dob' => 'datetime',
    ];

    /**
     * Get the user that related to the table.
     */
    public function userDetails()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getDateOfBirthAttribute()
    {
        return $this->dob ? date('d M, Y', strtotime($this->dob)) : '' ;
    }

    public function userEducation()
    {
        return $this->hasOne(EducationCategory::class, 'id', 'education_id');
    }

    public function userCountry()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function userCity()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function getUserProfileImageAttribute()
    {
        return ($this->profilepic && Storage::exists('public/profile_pic/thumbnails/' . $this->profilepic)) ? Storage::url('public/profile_pic/thumbnails/' . $this->profilepic) : asset('images/avatar.jpg');
    }
}
