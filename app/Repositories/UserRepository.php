<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\UserDetail;
use App\Traits\FileFunctions;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class UserRepository extends BaseRepository

{
    use FileFunctions;
    private $userDetail;
    /**
     * UserRepository constructor.
     *
     * @author Siji <cgann02@gmail.com>
     *
     * @param User $model
     */
    public function __construct(User $model, UserDetail $userDetail)
    {
        parent::__construct($model);
        $this->userDetail = $userDetail;
    }

    /**
     * Create model data
     *
     * @param [array | Request] $data - data to store
     * @return User
     */
    public function createNew($data)
    {
        if ($data instanceof Request) {
            $data = $data->all();
        }

        $userInput = Arr::only($data, $this->model->getFillable());

        $user = $this->model->create($userInput);

        $userDetailInput = Arr::only($data, $this->userDetail->getFillable());

        if($data['profilepic']) {
            $uploadDetails = $this->storeProfileImageFile($data['profilepic'], 'profile_pic', $user);

            $imageName = $uploadDetails['name'];
        }

        $userDetailInput = array_merge($userDetailInput, [
            'dob' => date('Y-m-d', strtotime($data['dob'])),
            'profilepic' => $imageName,
            'education_id' => $data['education'],
            'country_id' => $data['country'],
            'city_id' => $data['city']
        ]);

        $user->userDetails()->create($userDetailInput);

        return $user;
    }

    public function updateUserInfo($id, $data)
    {
        if ($id instanceof $this->model) {
            $user = $id;
        } else {
            $user = $this->model->find($id);
            if (empty($user)) {
                throw new NotFoundResourceException('User not found', 404);
            }
        }

        $userInput = Arr::only($data, $this->model->getFillable());

        $users = $user->update($userInput);

        $userDetailInput = Arr::only($data, $this->userDetail->getFillable());

        $userDetail = $user->userDetails;
        if($userDetail) {
            $imageName = $userDetail->profilepic;
        }

        if(isset($data['profilepic']) && $data['profilepic']) {

            if($userDetail && $userDetail->profilepic){
                $deleteFile = $this->deleteFile($user->userDetails->profilepic, 'public/profile_pic');

            }
            $uploadDetails = $this->storeProfileImageFile($data['profilepic'], 'profile_pic', $user);

            $imageName = $uploadDetails['name'];
        }

        $userDetailInput = array_merge($userDetailInput, [
            'dob' => date('Y-m-d', strtotime($data['dob'])),
            'profilepic' => $imageName,
            'education_id' => $data['education'],
            'country_id' => $data['country'],
            'city_id' => $data['city']
        ]);

        $user->userDetails()->update($userDetailInput);

        return $user;
    }
}