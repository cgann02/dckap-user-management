<?php

namespace App\Traits;

use Illuminate\Http\Testing\MimeType;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\File;

use Image;

trait FileFunctions
{

    /**
     * Generate the path with filename for uplading the file
     * Unique filename will be generated
     *
     * @param string $path
     * @param string $mimeType
     *
     * @return string
     */
    private function generatePath($path, $extension, $user)
    {
        $fullpath = '';
        do {
            $name = $this->generateName($extension, $user);
            $fullpath = rtrim($path, '/') . '/' . $name;
        } while (Storage::exists($fullpath));
        return $fullpath;
    }

    /**
     * Generate random name from a file
     *
     * @param String $extension
     * @return void
     */
    private function generateName($extension, $user)
    {
        $name = str_replace(' ', '', $user->name ) . '_' . $user->id;
        return $name . '.' . $extension;
    }

    private function storeProfileImageFile($file, $path, $user)
    {
        $originalName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension() ? $file->getClientOriginalExtension() : $file->extension();

        $fullPath = $this->generatePath($path, $extension, $user);
        $fileInfo = pathinfo($fullPath);

        $name = $fileInfo['basename'];
        if ($file instanceof File) {
            Storage::putFileAs('public/'.$path, $file, $name);
        } else {
            Storage::put($fullPath, $file);
        }

        $this->storeProfileThumbnailImageFile($file, 'public/profile_pic/thumbnails', $name);
        return [
            'name' => $name,
            'display_name' => $originalName
        ];
    }

    private function storeProfileThumbnailImageFile($file, $path, $name)
    {
        $fullPath = Storage::path($path . '/' . $name);

        if(!Storage::exists($path)) {
            mkdir(Storage::path($path), 666, true);

        }

        Image::make($file->getRealPath())->resize(50, 50)->save($fullPath);

        return true;
    }

    private function deleteFile ($file, $path)
    {
        Storage::delete($path.'/'.$file);
        Storage::delete($path.'/thumbnails/'.$file);
        return true;
    }

}