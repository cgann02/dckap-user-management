<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\City;
use App\Models\Country;
use Carbon\Carbon;
use GuzzleHttp\Client;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * insert data using API for each country 
         */
        $countries = Country::all();

        // foreach($countries as $key => $value) {

        //     $client = new Client();
        //     $response = $client->request('POST', 'https://countriesnow.space/api/v0.1/countries/cities', [
        //         'form_params' => [
        //             "country" => strtolower($value->name)
        //         ]
        //     ]);

        //     $response = $response->getBody()->getContents();
        //     $data = json_decode($response);

        //     foreach($data->data as $k => $cityName){
        //         $insertData[] = [
        //             'name' => $cityName,
        //             'country_id' => $value->id,
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ];
        //     }
        // }

        /**
         * manually insert data
         */
        $insertData = [
            [
                'name' => 'Kerala',
                'country_id' => 102,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Tamilnadu',
                'country_id' => 102,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'name' => 'Thrissur',
                'country_id' => 102,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'name' => 'Palakkad',
                'country_id' => 102,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'name' => 'Bangalore',
                'country_id' => 102,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'name' => 'Karnataka',
                'code' => 102,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        City::insert($insertData);

    }
}
