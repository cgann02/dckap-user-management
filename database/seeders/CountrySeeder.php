<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Country;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Country::truncate();
        Schema::enableForeignKeyConstraints();
        /* seeding  with third party api */
        $client = new Client();
        $response = $client->request('GET', 'https://countriesnow.space/api/v0.1/countries/capital');

        $response = $response->getBody()->getContents();
        $data = json_decode($response);

        foreach($data->data as $key => $country) {
            $insertData = [
                'name' => ucwords(strtolower($country->name)),
                'code' => $country->iso2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
            logger()->info($insertData);
            Country::create($insertData);
        }
        /* manual seeding */
        // if(count($insertData) < 1)
        // {
        //     $insertData = [
        //         [
        //             'name' => 'India',
        //             'code' => 'IN',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],
        //         [
        //             'name' => 'Canada',
        //             'code' => 'CA',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'China',
        //             'code' => 'CN',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'Egypt',
        //             'code' => 'EG',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'Germany',
        //             'code' => 'DE',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'Israel',
        //             'code' => 'IL',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'Italy',
        //             'code' => 'IT',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'Kuwait',
        //             'code' => 'KW',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'United Kingdom',
        //             'code' => 'UK',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'United Arab Emirates',
        //             'code' => 'AE',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],[
        //             'name' => 'United States',
        //             'code' => 'US',
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now()
        //         ],
        //     ];
        // }
        // Country::insert($insertData);

    }
}
