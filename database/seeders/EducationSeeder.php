<?php

namespace Database\Seeders;

use App\Models\EducationCategory;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'education' => '10th',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'education' => '12th',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'education' => 'Diploma',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'education' => 'BTech',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'education' => 'MTech',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'education' => 'ITI',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'education' => 'MBS',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'education' => 'UG',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'education' => 'PG',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        EducationCategory::insert($data);
    }
}
