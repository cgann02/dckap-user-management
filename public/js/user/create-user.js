$(document).ready(function () {

    $('#profilepic').change( function () {
        $('.alert').hide();
        var fileExtension = ['jpeg', 'jpg', 'png'],
            size = (this.files[0].size / 1024 / 1024).toFixed(2),
            isValid = true;

        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            isValid = false;
            $('.alert-danger span:first').html("Only '.jpeg','.jpg','.png' formats are allowed.");
        } else if (size > 4 ) {
            isValid = false;
            $('.alert-danger span:first').html("File too Big, please select a file less than 4mb");                
        }

        if(!isValid) {
            $('#profilepic').val("");

            $('.alert-danger').show();
            setTimeout( function () {
                $('.alert-danger').hide();
            }, 5000 );
        } else {
            document.getElementById('output').src = window.URL.createObjectURL(this.files[0]);
        $('#output').show();

        }

    });

    $("#dob").datepicker({ dateFormat: "dd-mm-yy",changeMonth: true,
    changeYear: true, maxDate: '-1d' });

    $('#country').select2({
        selectOnClose: true
    });

    $('#education, #city').select2();

    $('#country').on('change', function() {
        event.preventDefault();

        $.ajax({
            type: "GET",
            url: '/users/cities/' + $(this).val(),
            dataType: "json",
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content") },
            beforeSend: function() {
                $('div.alert').hide();
                $('#city').html('<option value="">Choose City</option>').addClass('no-data').select2();
                $('button[type="submit"]').attr('disabled', true);
            },
            success: function(result) {
                if(result.success) {
                    html = '<option value="">Choose City</option>';
                    $.each(result.data, function (key, value) {
                        html += '<option value="'+ key + '">' + value + '</option>';
                    });

                    $('#city').html(html).removeClass('no-data');
                    $('#city').select2();

                    $('button[type="submit"]').removeAttr('disabled');
                } else {
                    $('.alert-danger span:first').html(result.message);
                    $('.alert-danger').show();
                    $('#city').html('<option value="">Choose City</option>').addClass('no-data').select2();
                    setTimeout( function () {
                        $('.alert-danger').hide();
                    }, 5000 );
                }
            }
        });
    });

    $('input, select').on('change', function() {
        $(this).removeClass('is-invalid');
        $(this).siblings('span.invalid-feedback').hide();
    });
    $("#user-details").submit(function(e){
        e.preventDefault();

        $('div.alert, span .error').hide();

        if(!$(this).valid()) {
            return false;
        }

        var myform = document.getElementById("user-details");
        var fd = new FormData(myform);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: $(this).attr('data-url'),
            dataType: 'JSON',
            type: "POST",
            contentType: false,
            processData: false,
            data: fd,
            beforeSend: function() {
                $('div.alert, span .error').hide();
                $('button[type="submit"]').attr('disabled', true);
            },
            success: function (response) {
                if(response.success) {
                    $('.alert-success span:first').html(response.message);
                    $('.alert-success').show();
                }

                setTimeout( function () {
                    location.reload();
                }, 5000 );
            },
            error: function (error) {
                var response = JSON.parse(error.responseText);
                if (response.code == 422) {
                    $(".js-common-error")
                        .text("All inputs are required")
                        .removeClass("hide");
                    $.each(response.errors, function (key, value) {
                        $('[name="' + key + '"]').addClass("is-invalid");

                        var parentDiv = $('[name="' + key + '"]').parent('.input-group');

                        if(parentDiv.find('span[for="' + key + '"]').length) {
                            parentDiv.find('span[for="' + key + '"]').html(value).show();
                        } else {
                            parentDiv.append('<span for="' + key + '" class="error invalid-feedback">' + value + '</span>');
                        }
                                                        
                    });
                } else {
                    $(".alert-danger span:first").html("Something went wrong!!! Please try again");
                    $(".alert-danger").show();
                }
            },
            complete: function() {
                $('button[type="submit"]').removeAttr('disabled');
            }
        });
    });

    $('#user-details').validate({
        ignore: "",
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true
            },
            dob: {
                required: true,
                date: true
            },
            status: {
                required: true
            },
            education: {
                required: true
            },
            country: {
                required: true
            },
            city: {
                required: true
            },
            pincode: {
                required: true
            },
            profilepic: {
                required: true
            }
        },
    
        errorElement: 'span',
        errorPlacement: function (error, element) {
            var n = element.attr("name");

            error.addClass('invalid-feedback');
            element.closest('.input-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});