$(document).on('click', '.js-delete-btn', function() {
    let _this = $(this),
        user_id = _this.attr('data-id');
    if(user_id) {
        Swal.fire({
            title: 'Do you want to delete this user?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({

                    type : "GET",
                    url : userDeleteURL + '/' + user_id,
                    processData: false,
                    contentType: false,
                    dataType: 'json',           
        
                    success :function(result){
                        if(result.success) {
                            Swal.fire(result.message, '', 'success')
                            $('.alert-success span:first').html(result.message);
                            $('.alert-success').show();

                            $('#user-details-datatable').DataTable().ajax.reload();
                        } else {
                            Swal.fire(result.message, '', 'error')
                            $('.alert-danger span:first').html(result.message);
                            $('.alert-danger').show();
                            $('#user-details-datatable').DataTable().ajax.reload();
                        }
                    },
                    error: function(response) {
                        Swal.fire(response.responseJSON.message, '', 'error')
                    },
                    complete: function() {
                        setTimeout( function () {
                            $('.alert').hide();
                        }, 2000 );
                    }
                });
            } else if (result.isDismissed) {
              Swal.fire('Service not deleted!', '', 'info')
            }
        })
    }

});

$('#user-name, #user-email, #user-age').select2({
    selectOnClose: true
});

$(document).on('click', '.buttons-reset', function(){
    $('#user-name, #user-email, #user-age').val('').trigger('change');
    $('#filter-form')[0].reset();
})