@extends('layouts.login-layout')
@section('content')
    <div class="page-header align-items-start min-vh-100">
      <span class="mask bg-gradient-dark opacity-6"></span>
      <div class="container my-auto">
        <div class="row">
                        
          <div class="col-lg-4 col-md-8 col-12 mx-auto">
            <!-- <div class="card z-index-0 fadeIn3 fadeInBottom"> -->
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                  <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">Sign in</h4>
                  <div class="row mt-3">
                    <div class="col-2 text-center ms-auto">
                      
                    </div>
                    <div class="col-2 text-center px-1">
                        <a class="btn btn-link px-3" href="{{ route('login.google') }}">
                        <i class="fa fa-google text-white text-lg"></i>
                      </a>
                    </div>
                    <div class="col-2 text-center me-auto">
                      
                    </div>
                  </div>
                </div>
              <!-- </div> -->
            </div>
          </div>
        </div>
      </div>
      
    </div>
@endsection