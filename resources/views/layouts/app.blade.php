<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.header')
    <body class="g-sidenav-show  bg-gray-200">
        @include('layouts.sidebar')
        <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ps">
            @include('layouts.nav-bar')
            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-12">
                    @if (session('commonError'))
                        <div class="alert alert-warning alert-dismissible text-white" role="alert">
                            <span class="text-sm">{{ session('commonError') }}</span>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                        <div class="alert alert-danger alert-dismissible text-white" role="alert" style="display: none;">
                            <span class="text-sm">A simple danger alert with <a href="javascript:;" class="alert-link text-white">an example link</a>. Give it a click if you like.</span>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
            
                        <div class="alert alert-success alert-dismissible text-white" role="alert" style="display: none;">
                            <span class="text-sm">A simple success alert with <a href="javascript:;" class="alert-link text-white">an example link</a>. Give it a click if you like.</span>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                @yield('content')
            </div>
        </main>
        
    </body>
    @include('layouts.footer-js')
</html>
