
  <!--   Core JS Files   -->
 <script src="{{ asset('/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('/js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('/js/plugins/smooth-scrollbar.min.js') }}"></script>
 
  <!-- Github buttons -->
  <script async defer src="{{ asset('js/plugins/buttons.js')}}"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('/js/material-dashboard.min.js')}}?v=3.0.4"></script>

  @stack('js')