<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  @include('layouts.header')


  <body class="bg-gray-200">
  <div class="position-sticky z-index-sticky top-2">
    <div class="row">
      <div class="col-12">
        @if (session('commonError'))
          <div class="alert alert-warning alert-dismissible text-white" role="alert">
            <span class="text-sm">{{ session('commonError') }}</span>
            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        @endif
      </div>
    </div>
  </div>
    <main class="main-content  mt-0">
      @yield('content')
    
    </main>
  </body>
  @include('layouts.footer-js')
</html>