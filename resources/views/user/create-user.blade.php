@extends('layouts.app')
@section('pageTitle','New User')

@push('css')
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />

<style>
input.radio {
    margin-left: 3% !important;
}

select.is-invalid + .select2 .select2-selection {
    /* background-color: red !important; */
    border-color: red !important;
}
.is-invalid {
    border-color: red !important;
}

select.no-data + .select2 .select2-selection{
    pointer-events: none !important;
}
</style>
@endpush
@section('content')


    <div class="row col-6 center">
 
        <!-- <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0"> -->
            
            <div class="card" style="margin-left: 50%;">
                <div class="card-body">
                    <form role="form text-left" id="user-details" data-url="{{ route('users.store') }}">
                        <label class="form-label">Name</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="text" class="form-control" name="name" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)">
                        </div>

                        <label class="form-label">Email</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="email" class="form-control" name="email">
                        </div>

                        <label class="form-label">Address</label>
                        <div class="input-group input-group-outline my-3">
                            <textarea name="address" class="form-control"></textarea>
                        </div>

                        <label class="form-label">Date of Birth</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="text" id="dob" name="dob" class="form-control" placeholder="dd-mm-yyyy">
                        </div>

                        <label class="form-label">Status</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="radio" class="radio" id="active" name="status" value="1" checked> Active
                            <input type="radio" class="radio" id="inactive" name="status" value="0">Inactive

                        </div>

                        <label class="form-label">Education</label>
                        <div class="input-group input-group-outline my-3">
                            <select id="education" name="education" class="form-control">
                                <option value="" disabled selected> Choose education</option>
                                @foreach($education as $key => $edu)
                                    <option value="{{$edu->id}}">{{$edu->education}}</option>
                                @endforeach
                            </select>
                        </div>

                        <label class="form-label">Country</label>
                        <div class="input-group input-group-outline my-3">
                            <select id="country" name="country" class="form-control">
                                <option value=""> Choose country </option>
                                @foreach($country as $key => $countryDetails)
                                    <option value="{{$countryDetails->id}}">{{$countryDetails->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <label class="form-label">City</label>
                        <div class="input-group input-group-outline my-3">
                            <select id="city" name="city" class="form-control no-data">
                                <option value=""> Choose city </option>
                                
                            </select>
                        </div>

                        <label class="form-label">Pin Code</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="text" name="pincode" class="form-control">
                        </div>

                        <label class="form-label">Profile Pic</label>
                        <div class="input-group input-group-outline my-3">
                            <img id="output" src="" width="100" height="100" style="display:none;">

                            <input name="profilepic" type="file"   id="profilepic">

                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-round bg-gradient-info btn-lg w-100 mt-4 mb-0">Save</button>
                        </div>
                            
                    </form>
                </div>
            </div>
            
        <!-- </div> -->
    </div>

@endsection


@push('js')
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<script src="{{ asset('js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/user/create-user.js')}}"></script>
@endpush