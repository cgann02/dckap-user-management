@extends('layouts.app')
@section('pageTitle','Users List')
@push('css')
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
@endpush
@section('content')


    <div class="row">
        
    <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            <!-- <span class="app-name">Users List</span> -->
            <div class="col-12" style="text-align: right;">
            <a href="{{route('users.create')}}" class="btn btn-info">+ Create User</a>

            </div>
            <div class="flex justify-center pt-8 sm:justify-start sm:pt-0 common-success" style="display:none;">
                    <span></span>
                </div>
            <div class="card-header">
                <form class="" id="filter-form">
                    <!-- <label class="mr-sm-2 form-label" for="admin-user-search">Search by:</label>
                    <input type="text" class="form-control search mb-2 mr-sm-2 mb-sm-0" id="filter-search"
                        placeholder="Search ..."> -->

                    <div class="row" style="width: auto !important;">
                        <div class="col-md-4">
                            <label class="form-label" for="">Search by: </label>
                            <input type="text" class="form-control search mb-2 mr-sm-2 mb-sm-0" id="filter-search" placeholder="Search ...">
                        </div>
                        <div class="col-md-4">
                            <label class="form-label" for="admin-user-search">Name </label>
                            <select id="user-name" name="user_name" class="form-control">
                                <option value=""> Select User </option>
                                @foreach($users as $key => $user)
                                    <option value="{{$user->name}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label" for="admin-user-search">Email </label>
                            <select id="user-email" name="user_email" class="form-control">
                                <option value=""> Select User </option>
                                @foreach($users as $key => $user)
                                    <option value="{{$user->email}}">{{$user->email}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label" for="admin-user-search">Age </label>
                            <select id="user-age" name="user_age" class="form-control">
                                <option value=""> Select User </option>
                                @for($i=0; $i <= 100; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label" for="admin-user-search">Status </label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter-status">
                                <option value="">All Status</option>

                                <option value="1">Active</option>
                                <option value="0">Inactive</option>

                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <!-- DataTable init -->
            <div class="table-responsive">
                
                @component('components.datatable',compact('dataTable')) @endcomponent
            </div>
        </div>
    </div>

@endsection

@push('js')
<script src="{{ asset('js/sweetalert2@11.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<script>
    const userDeleteURL = "{{ route('users.details.delete') }}"; 
</script>
<script src="{{ asset('js/user/user-list.js') }}"></script>

@endpush