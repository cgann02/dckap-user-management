@extends('layouts.app')
@section('pageTitle','User Details')

@section('content')
    <div class="row col-6">
        <div  style="margin-left: 9%;">
            <div class="card mb-4 " style="margin-left: 50%;">
                <div class="d-flex">
                    <div class="icon icon-shape icon-lg shadow text-center border-radius-xl mt-n3 ms-4">
                    <img src="{{($userDetails) ? $userDetails->user_profile_image : asset('images/avatar.jpg')}}" style="width: 64px;height: 64px;" class="material-icons opacity-10">
                    </div>
                    <!-- <h6 class="mt-3 mb-2 ms-3 ">Sales by Country</h6> -->
                </div>
                <div class="card-body p-3">
                    <div class="row">
                        <div class="">
                            <div class="table-responsive">
                                <table class="table align-items-center ">
                                    <tbody>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">Name: </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{ $user->name }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">Email : </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{ $user->email?:'-' }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-30" style="white-space: inherit;">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">Address : </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{ ($userDetails && $userDetails->address) ? $userDetails->address : '-' }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">DOB : </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{ ($userDetails && $userDetails->date_of_birth) ? $userDetails->date_of_birth : '-' }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">Status : </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 ">
                                                            @if($user->status == 1)
                                                                <span class="badge badge-sm bg-gradient-success">Active</span>
                                                            @else
                                                                <span class="badge badge-sm bg-gradient-secondary">Inactive</span>
                                                            @endif
                                                        </h6>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">Education : </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{ ($userDetails && $userDetails->userEducation) ? $userDetails->userEducation->education : '-' }}</h6>
                                                    </div>
                                                </div>
                                            </td>                                            
                                        </tr>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">Country : </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{ ($userDetails && $userDetails->userCountry) ? $userDetails->userCountry->name : '-' }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">City : </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{ ($userDetails && $userDetails->userCity) ? $userDetails->userCity->name : '-' }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-30">
                                                <div class="d-flex px-2 py-1 align-items-center">
                                                    <div class="col-4">
                                                    <p class="text-xs font-weight-bold mb-0 ">Pin Code: </p>
                                                    </div>
                                                    <div class="ms-4">
                                                        
                                                        <h6 class="text-sm font-weight-normal mb-0 "> {{($userDetails && $userDetails->pincode) ? $userDetails->pincode : '-' }} </h6>
                                                    </div>
                                                </div>
                                            </td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="{{ route('users.list') }}" class="btn btn-round bg-gradient-info btn-lg w-100 mt-4 mb-0">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection