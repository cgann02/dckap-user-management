<?php

use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\Auth\HomeController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthenticationController::class, 'login'])->name('login');



Route::group(['middleware' => 'guest'], function () {
        Route::get('login/google', [AuthenticationController::class, 'redirectToGoogle'])->name('login.google');
        Route::get('login/google/callback', [AuthenticationController::class, 'handleGoogleCallback'])->name('login.google.callback');
});

Route::group(['middleware' => 'users'], function () {

        Route::get('dashboard', [HomeController::class, 'index'])->name('home');

        /**
         * User Resources with CRUD operation
         */
        // Route::resource('users', UserController::class);

        Route::get('users', [UserController::class, 'index'])->name('users.list');

        Route::get('users/create', [UserController::class, 'create'])->name('users.create');

        Route::post('users/store', [UserController::class, 'store'])->name('users.store');
        
        Route::get('users/cities/{countryId}', [UserController::class, 'getCountryBasedCities'])->name('users.cities');

        Route::get('users/{id}/edit', [UserController::class, 'edit'])->name('users.details.edit');

        Route::post('users/update', [UserController::class, 'update'])->name('users.details.update');

        Route::get('users/delete/{id?}', [UserController::class, 'delete'])->name('users.details.delete');

        Route::get('users/{id}/view', [UserController::class, 'show'])->name('users.details.show');

        Route::any('/logout', [AuthenticationController::class, 'logout'])->name('logout');

});