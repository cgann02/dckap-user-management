-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2022 at 07:51 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'Kerala', 102, '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(2, 'Tamilnadu', 102, '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(3, 'Thrissur', 102, '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(4, 'Palakkad', 102, '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(5, 'Bangalore', 102, '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(6, 'Karnataka', 102, '2022-10-19 12:40:14', '2022-10-19 12:40:14');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 'AF', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(2, 'Aland Islands', 'AX', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(3, 'Albania', 'AL', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(4, 'Algeria', 'DZ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(5, 'American Samoa', 'AS', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(6, 'Andorra', 'AD', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(7, 'Angola', 'AO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(8, 'Anguilla', 'AI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(9, 'Antarctica', 'AQ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(10, 'Antigua And Barbuda', 'AG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(11, 'Argentina', 'AR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(12, 'Armenia', 'AM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(13, 'Aruba', 'AW', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(14, 'Australia', 'AU', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(15, 'Austria', 'AT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(16, 'Azerbaijan', 'AZ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(17, 'Bahamas', 'BS', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(18, 'Bahrain', 'BH', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(19, 'Bangladesh', 'BD', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(20, 'Barbados', 'BB', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(21, 'Belarus', 'BY', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(22, 'Belgium', 'BE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(23, 'Belize', 'BZ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(24, 'Benin', 'BJ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(25, 'Bermuda', 'BM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(26, 'Bhutan', 'BT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(27, 'Bolivia', 'BO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(28, 'Bonaire, Saint Eustatius And Saba ', 'BQ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(29, 'Bosnia And Herzegovina', 'BA', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(30, 'Botswana', 'BW', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(31, 'Bouvet Island', 'BV', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(32, 'Brazil', 'BR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(33, 'British Indian Ocean Territory', 'IO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(34, 'British Virgin Islands', 'VG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(35, 'Brunei', 'BN', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(36, 'Bulgaria', 'BG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(37, 'Burkina Faso', 'BF', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(38, 'Burundi', 'BI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(39, 'Cambodia', 'KH', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(40, 'Cameroon', 'CM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(41, 'Canada', 'CA', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(42, 'Cape Verde', 'CV', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(43, 'Cayman Islands', 'KY', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(44, 'Central African Republic', 'CF', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(45, 'Chad', 'TD', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(46, 'Chile', 'CL', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(47, 'China', 'CN', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(48, 'Christmas Island', 'CX', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(49, 'Cocos (keeling) Islands', 'CC', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(50, 'Cocos Islands', 'CC', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(51, 'Colombia', 'CO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(52, 'Comoros', 'KM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(53, 'Congo', 'CG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(54, 'Cook Islands', 'CK', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(55, 'Costa Rica', 'CR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(56, 'Croatia', 'HR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(57, 'Cuba', 'CU', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(58, 'Curacao', 'CW', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(59, 'Cyprus', 'CY', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(60, 'Czech Republic', 'CZ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(61, 'Democratic Republic Of The Congo', 'CD', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(62, 'Denmark', 'DK', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(63, 'Djibouti', 'DJ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(64, 'Dominica', 'DM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(65, 'Dominican Republic', 'DO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(66, 'Ecuador', 'EC', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(67, 'Egypt', 'EG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(68, 'El Salvador', 'SV', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(69, 'Equatorial Guinea', 'GQ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(70, 'Eritrea', 'ER', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(71, 'Estonia', 'EE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(72, 'Ethiopia', 'ET', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(73, 'Falkland Islands', 'FK', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(74, 'Faroe Islands', 'FO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(75, 'Fiji', 'FJ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(76, 'Finland', 'FI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(77, 'France', 'FR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(78, 'French Polynesia', 'PF', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(79, 'French Southern Territories', 'TF', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(80, 'Gabon', 'GA', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(81, 'Gambia', 'GM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(82, 'Georgia', 'GE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(83, 'Germany', 'DE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(84, 'Ghana', 'GH', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(85, 'Gibraltar', 'GI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(86, 'Greece', 'GR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(87, 'Greenland', 'GL', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(88, 'Grenada', 'GD', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(89, 'Guadeloupe', 'GP', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(90, 'Guam', 'GU', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(91, 'Guatemala', 'GT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(92, 'Guernsey', 'GG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(93, 'Guinea', 'GN', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(94, 'Guinea-bissau', 'GW', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(95, 'Guyana', 'GY', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(96, 'Haiti', 'HT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(97, 'Heard Island And Mcdonald Islands', 'HM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(98, 'Honduras', 'HN', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(99, 'Hong Kong', 'HK', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(100, 'Hungary', 'HU', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(101, 'Iceland', 'IS', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(102, 'India', 'IN', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(103, 'Indonesia', 'ID', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(104, 'Iran', 'IR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(105, 'Iraq', 'IQ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(106, 'Ireland', 'IE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(107, 'Isle Of Man', 'IM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(108, 'Israel', 'IL', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(109, 'Italy', 'IT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(110, 'Ivory Coast', 'CI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(111, 'Jamaica', 'JM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(112, 'Japan', 'JP', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(113, 'Jersey', 'JE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(114, 'Jordan', 'JO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(115, 'Kazakhstan', 'KZ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(116, 'Kenya', 'KE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(117, 'Kiribati', 'KI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(118, 'Kosovo', 'XK', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(119, 'Kuwait', 'KW', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(120, 'Kyrgyzstan', 'KG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(121, 'Laos', 'LA', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(122, 'Latvia', 'LV', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(123, 'Lebanon', 'LB', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(124, 'Lesotho', 'LS', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(125, 'Liberia', 'LR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(126, 'Libya', 'LY', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(127, 'Liechtenstein', 'LI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(128, 'Lithuania', 'LT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(129, 'Luxembourg', 'LU', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(130, 'Macau', 'MO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(131, 'Macedonia', 'MK', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(132, 'Madagascar', 'MG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(133, 'Malawi', 'MW', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(134, 'Malaysia', 'MY', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(135, 'Maldives', 'MV', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(136, 'Mali', 'ML', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(137, 'Malta', 'MT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(138, 'Marshall Islands', 'MH', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(139, 'Martinique', 'MQ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(140, 'Mauritania', 'MR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(141, 'Mauritius', 'MU', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(142, 'Mayotte', 'YT', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(143, 'Mexico', 'MX', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(144, 'Micronesia', 'FM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(145, 'Moldova', 'MD', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(146, 'Monaco', 'MC', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(147, 'Mongolia', 'MN', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(148, 'Montenegro', 'ME', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(149, 'Montserrat', 'MS', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(150, 'Morocco', 'MA', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(151, 'Mozambique', 'MZ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(152, 'Myanmar', 'MM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(153, 'Namibia', 'NA', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(154, 'Nauru', 'NR', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(155, 'Nepal', 'NP', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(156, 'Netherlands', 'NL', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(157, 'New Caledonia', 'NC', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(158, 'New Zealand', 'NZ', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(159, 'Nicaragua', 'NI', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(160, 'Niger', 'NE', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(161, 'Nigeria', 'NG', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(162, 'Niue', 'NU', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(163, 'Norfolk Island', 'NF', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(164, 'North Korea', 'KP', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(165, 'Northern Mariana Islands', 'MP', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(166, 'Norway', 'NO', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(167, 'Oman', 'OM', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(168, 'Pakistan', 'PK', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(169, 'Palau', 'PW', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(170, 'Palestinian Territory', 'PS', '2022-10-19 12:40:13', '2022-10-19 12:40:13'),
(171, 'Panama', 'PA', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(172, 'Papua New Guinea', 'PG', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(173, 'Paraguay', 'PY', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(174, 'Peru', 'PE', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(175, 'Philippines', 'PH', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(176, 'Pitcairn', 'PN', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(177, 'Poland', 'PL', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(178, 'Portugal', 'PT', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(179, 'Puerto Rico', 'PR', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(180, 'Qatar', 'QA', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(181, 'Romania', 'RO', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(182, 'Russia', 'RU', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(183, 'Rwanda', 'RW', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(184, 'R??union', 'RE', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(185, 'Saint Barthelemy', 'BL', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(186, 'Saint Helena', 'SH', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(187, 'Saint Kitts And Nevis', 'KN', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(188, 'Saint Lucia', 'LC', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(189, 'Saint Martin', 'MF', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(190, 'Saint Pierre And Miquelon', 'PM', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(191, 'Saint Vincent And The Grenadines', 'VC', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(192, 'Samoa', 'WS', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(193, 'San Marino', 'SM', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(194, 'Sao Tome And Principe', 'ST', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(195, 'Saudi Arabia', 'SA', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(196, 'Senegal', 'SN', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(197, 'Serbia', 'RS', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(198, 'Seychelles', 'SC', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(199, 'Sierra Leone', 'SL', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(200, 'Singapore', 'SG', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(201, 'Sint Maarten', 'SX', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(202, 'Slovakia', 'SK', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(203, 'Slovenia', 'SI', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(204, 'Solomon Islands', 'SB', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(205, 'Somalia', 'SO', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(206, 'South Africa', 'ZA', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(207, 'South Georgia And The South Sandwich Islands', 'GS', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(208, 'South Korea', 'KR', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(209, 'South Sudan', 'SS', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(210, 'Spain', 'ES', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(211, 'Sri Lanka', 'LK', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(212, 'Sudan', 'SD', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(213, 'Suriname', 'SR', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(214, 'Svalbard And Jan Mayen', 'SJ', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(215, 'Swaziland', 'SZ', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(216, 'Sweden', 'SE', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(217, 'Switzerland', 'CH', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(218, 'Syria', 'SY', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(219, 'Taiwan', 'TW', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(220, 'Tajikistan', 'TJ', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(221, 'Tanzania', 'TZ', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(222, 'Thailand', 'TH', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(223, 'Timor-leste', 'TL', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(224, 'Togo', 'TG', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(225, 'Tokelau', 'TK', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(226, 'Tonga', 'TO', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(227, 'Trinidad And Tobago', 'TT', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(228, 'Tunisia', 'TN', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(229, 'Turkey', 'TR', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(230, 'Turkmenistan', 'TM', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(231, 'Turks And Caicos Islands', 'TC', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(232, 'Tuvalu', 'TV', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(233, 'U.s. Virgin Islands', 'VI', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(234, 'Uganda', 'UG', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(235, 'Ukraine', 'UA', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(236, 'United Arab Emirates', 'AE', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(237, 'United Kingdom', 'GB', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(238, 'United States', 'US', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(239, 'United States Minor Outlying Islands', 'UM', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(240, 'Uruguay', 'UY', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(241, 'Uzbekistan', 'UZ', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(242, 'Vanuatu', 'VU', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(243, 'Vatican', 'VA', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(244, 'Vatican City State (holy See)', 'VA', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(245, 'Venezuela', 'VE', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(246, 'Vietnam', 'VN', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(247, 'Wallis And Futuna', 'WF', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(248, 'Western Sahara', 'EH', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(249, 'Yemen', 'YE', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(250, 'Zambia', 'ZM', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(251, 'Zimbabwe', 'ZW', '2022-10-19 12:40:14', '2022-10-19 12:40:14');

-- --------------------------------------------------------

--
-- Table structure for table `education_categories`
--

CREATE TABLE `education_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `education_categories`
--

INSERT INTO `education_categories` (`id`, `education`, `created_at`, `updated_at`) VALUES
(1, '10th', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(2, '12th', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(3, 'Diploma', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(4, 'BTech', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(5, 'MTech', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(6, 'ITI', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(7, 'MBS', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(8, 'UG', '2022-10-19 12:40:14', '2022-10-19 12:40:14'),
(9, 'PG', '2022-10-19 12:40:14', '2022-10-19 12:40:14');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_10_15_071552_create_education_categories_table', 1),
(6, '2022_10_15_071952_create_countries_table', 1),
(7, '2022_10_15_071954_create_cities_table', 1),
(8, '2022_10_15_071958_create_user_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Siji Ann', 'cgann02@gmail.com', NULL, NULL, 1, NULL, '2022-10-19 12:40:24', '2022-10-19 12:40:24'),
(3, 'Siji Ann Thomas', 'annthomas.acg94@gmail.com', NULL, NULL, 1, NULL, '2022-10-19 12:43:42', '2022-10-19 12:43:42'),
(4, 'Azalia Randolph', 'jiruci@mailinator.com', NULL, NULL, 0, NULL, '2022-10-19 22:40:41', '2022-10-19 22:40:41'),
(8, 'Dacey Harrell', 'bevisaj111@mailinator.com', NULL, NULL, 0, NULL, '2022-10-20 02:52:09', '2022-10-20 02:52:09'),
(9, 'Xander Klein', 'qofu@mailinator.com', NULL, NULL, 0, NULL, '2022-10-20 04:50:06', '2022-10-20 04:50:06'),
(10, 'Harriet Webster', 'vygac@mailinator.com', NULL, NULL, 0, NULL, '2022-10-20 04:52:50', '2022-10-20 04:52:50'),
(11, 'Jasmine Guzman', 'helyxyxone@mailinator.com', NULL, NULL, 0, NULL, '2022-10-20 04:53:52', '2022-10-20 04:53:52'),
(12, 'Aurelia Parrish', 'taqehu@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 04:54:36', '2022-10-20 04:54:36'),
(13, 'Galvin Ashley', 'lafomuqusa@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 04:57:52', '2022-10-20 04:57:52'),
(14, 'Galvin Ashley', 'lafomuqusa1@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 04:59:21', '2022-10-20 04:59:21'),
(15, 'Galvin Ashley', 'lafomuqusa11@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 04:59:59', '2022-10-20 04:59:59'),
(16, 'Galvin Ashley', 'lafomuqusa121@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 05:01:59', '2022-10-20 05:01:59'),
(17, 'Blake Hunt', 'lohaqut@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 05:02:55', '2022-10-20 05:02:55'),
(18, 'Cara Dunn', 'xyfa@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 05:04:32', '2022-10-20 05:04:32'),
(19, 'Zeph Delgado', 'kanufyci@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 05:07:01', '2022-10-20 05:07:01'),
(20, 'Plato Russo', 'taqy@mailinator.com', NULL, NULL, 0, NULL, '2022-10-20 05:07:48', '2022-10-20 05:07:48'),
(21, 'Addison Bender', 'buzadekedo@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 05:10:04', '2022-10-20 05:10:04'),
(22, 'Yvette Flores', 'lycexepege@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 08:03:33', '2022-10-20 08:03:33'),
(23, 'Yvette Flores', 'lycexepege1@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 08:04:22', '2022-10-20 08:04:22'),
(24, 'Yvette Flores', 'lycexepege12@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 08:05:07', '2022-10-20 08:05:07'),
(25, 'Venus Preston', 'wysopysuny@mailinator.com', NULL, NULL, 0, NULL, '2022-10-20 08:06:24', '2022-10-20 08:06:24'),
(26, 'Zelda Le', 'wyvuzybi@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 08:08:10', '2022-10-20 08:08:10'),
(27, 'Nathan Kramer', 'ricolupapy11@mailinator.com', NULL, NULL, 1, NULL, '2022-10-20 08:08:57', '2022-10-20 08:52:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `dob` date DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education_id` bigint(20) UNSIGNED DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profilepic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` bigint(20) UNSIGNED DEFAULT NULL,
  `city_id` bigint(20) UNSIGNED DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `dob`, `address`, `education_id`, `pincode`, `profilepic`, `country_id`, `city_id`, `google_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-10-19 12:40:24', '2022-10-19 12:40:24'),
(3, 3, '1994-10-09', 'We at Creative Tim have always wanted to deliver great tools to all the web developers. We want to see better websites and web apps on the internet.\n\nYou can try out our Material Dashboard Builder here.', 2, '678683', 'Siji Ann Thomas_3.png', 102, 1, NULL, '2022-10-19 12:43:42', '2022-10-19 12:43:42'),
(4, 4, '2022-10-09', 'Harum reiciendis odi', 7, 'Quo voluptate cum ap', 'Azalia Randolph_4.png', 102, 2, NULL, '2022-10-19 22:40:41', '2022-10-19 22:40:41'),
(5, 8, '2022-10-03', 'Dolore similique mag', 1, 'Est aliquip eligendi', 'Dacey Harrell_8.png', 102, 2, NULL, '2022-10-20 02:52:09', '2022-10-20 02:52:09'),
(6, 9, '2022-10-02', 'Nostrum fuga Dolore', 7, '678683', 'Xander Klein_9.png', 102, 1, NULL, '2022-10-20 04:50:07', '2022-10-20 04:50:07'),
(7, 10, '2022-10-02', 'In nihil sed sequi e', 9, 'Sunt et quasi magna', 'Harriet Webster_10.png', 102, 2, NULL, '2022-10-20 04:52:50', '2022-10-20 04:52:50'),
(8, 11, '2022-10-10', 'Provident qui et a', 4, 'Quisquam consequatur', 'Jasmine Guzman_11.png', 102, 2, NULL, '2022-10-20 04:53:52', '2022-10-20 04:53:52'),
(9, 12, '2022-10-02', 'Amet eum non volupt', 4, 'Quos ut quia magnam', 'Aurelia Parrish_12.jpeg', 102, 1, NULL, '2022-10-20 04:54:36', '2022-10-20 04:54:36'),
(10, 16, '2022-10-09', 'Vero fugiat aute ab', 3, 'Facilis reiciendis q', 'Galvin Ashley_16.png', 102, 2, NULL, '2022-10-20 05:01:59', '2022-10-20 05:01:59'),
(11, 17, '2022-10-02', 'Pariatur Similique', 5, 'Est anim placeat e', 'Blake Hunt_17.png', 102, 2, NULL, '2022-10-20 05:02:55', '2022-10-20 05:02:55'),
(12, 18, '2022-10-09', 'Qui officiis tempora', 7, 'Tenetur architecto a', 'Cara Dunn_18.png', 102, 2, NULL, '2022-10-20 05:04:32', '2022-10-20 05:04:32'),
(13, 19, '2022-10-03', 'Exercitation lorem a', 7, 'Non dolores laborum', 'Zeph Delgado_19.png', 102, 2, NULL, '2022-10-20 05:07:01', '2022-10-20 05:07:01'),
(14, 20, '2022-10-09', 'Quia velit aut amet', 2, 'Alias quis eius porr', 'Plato Russo_20.jpeg', 102, 2, NULL, '2022-10-20 05:07:48', '2022-10-20 05:07:48'),
(15, 21, '2022-10-02', 'Iste eum dolor cillu', 7, 'Ratione commodi vita', 'Addison Bender_21.png', 102, 2, NULL, '2022-10-20 05:10:04', '2022-10-20 05:10:04'),
(16, 24, '2022-10-02', 'Libero cum occaecat', 1, 'Iure nesciunt corpo', 'YvetteFlores_24.png', 102, 3, NULL, '2022-10-20 08:05:08', '2022-10-20 08:05:08'),
(17, 25, '2022-10-09', 'Id voluptatibus sed', 3, 'In deserunt aute qui', 'VenusPreston_25.png', 102, 2, NULL, '2022-10-20 08:06:24', '2022-10-20 08:06:24'),
(18, 26, '2022-10-02', 'Eligendi natus imped', 3, 'In officiis ea perfe', 'ZeldaLe_26.png', 102, 4, NULL, '2022-10-20 08:08:11', '2022-10-20 08:08:11'),
(19, 27, '2022-10-02', 'Perferendis et qui c', 4, 'Laborum Illum aut', 'NathanKramer_27.png', 102, 2, NULL, '2022-10-20 08:08:58', '2022-10-20 08:53:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_country_id_foreign` (`country_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education_categories`
--
ALTER TABLE `education_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_details_user_id_foreign` (`user_id`),
  ADD KEY `user_details_education_id_foreign` (`education_id`),
  ADD KEY `user_details_country_id_foreign` (`country_id`),
  ADD KEY `user_details_city_id_foreign` (`city_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `education_categories`
--
ALTER TABLE `education_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_details_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_details_education_id_foreign` FOREIGN KEY (`education_id`) REFERENCES `education_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
